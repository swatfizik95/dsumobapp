﻿using System;
using DsuMobApp.Model;
using DsuMobApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DsuMobApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VoteShowPage : ContentPage
    {
        public VoteShowPage(Vote vote)
        {
            InitializeComponent();
            //
            BindingContext = new VoteShowViewModel(vote);
        }

        public VoteShowPage(VoteViewModel vote)
        {
            InitializeComponent();
            //
            BindingContext = new VoteShowViewModel(vote);
        }
    }
}