﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DsuMobApp.Model;
using DsuMobApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DsuMobApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NotificationPage : ContentPage
	{
		public NotificationPage (Notification notification)
		{
			InitializeComponent ();

            this.BindingContext = new NotificationViewModel(notification);
        }
	}
}