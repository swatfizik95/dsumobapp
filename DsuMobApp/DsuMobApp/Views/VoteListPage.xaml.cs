﻿using DsuMobApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DsuMobApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class VoteListPage : ContentPage
	{
		public VoteListPage()
		{
			InitializeComponent();

            BindingContext = new VoteListViewModel(Navigation);
        }
	}
}