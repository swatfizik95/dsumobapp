﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DsuMobApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DsuMobApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TestPage : ContentPage
	{
		public TestPage ()
		{
			InitializeComponent ();

            BindingContext = new TestViewModel();
        }

        private void Button_OnClicked(object sender, EventArgs e)
        {
            Application.Current.MainPage.DisplayAlert("test", "test", "aa");
        }
    }
}