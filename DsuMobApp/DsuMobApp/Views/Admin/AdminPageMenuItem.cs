﻿using System;
using Xamarin.Forms;

namespace DsuMobApp.Views.Admin
{
    public class AdminPageMenuItem
    {
        public AdminPageMenuItem()
        {
            TargetType = typeof(AdminPageDetail);
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public Color TextColor { get; set; }
        public Type TargetType { get; set; }
    }
}