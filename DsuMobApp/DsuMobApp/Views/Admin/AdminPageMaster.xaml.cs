﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using DsuMobApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DsuMobApp.Views.Admin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdminPageMaster : ContentPage
    {
        public ListView ListView;

        public AdminPageMaster()
        {
            InitializeComponent();

            BindingContext = new AdminPageMasterViewModel();
            ListView = MenuItemsListView;
        }

        class AdminPageMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<AdminPageMenuItem> MenuItems { get; set; }

            public AdminPageMasterViewModel()
            {
                MenuItems = new ObservableCollection<AdminPageMenuItem>(new[]
                {
                    new AdminPageMenuItem { Id = 0, Title = "Создать уведомление", TargetType = typeof(NotificationCreatePage)},
                    new AdminPageMenuItem { Id = 1, Title = "Создать голосование", TargetType = typeof(VoteCreatePage)},
                    new AdminPageMenuItem { Id = 2, Title = "Выход", TargetType = typeof(LogoutPage), TextColor = Color.FromRgb(178, 51, 51)},
                });
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}