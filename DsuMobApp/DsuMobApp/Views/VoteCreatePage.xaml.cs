﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DsuMobApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VoteCreatePage : ContentPage
    {
        public VoteCreatePage()
        {
            InitializeComponent();
        }
    }
}