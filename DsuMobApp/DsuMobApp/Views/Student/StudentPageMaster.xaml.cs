﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DsuMobApp.Views.Student
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StudentPageMaster : ContentPage
    {
        public ListView ListView;

        public StudentPageMaster()
        {
            InitializeComponent();

            BindingContext = new StudentPageMasterViewModel();
            ListView = MenuItemsListView;
        }

        class StudentPageMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<StudentPageMenuItem> MenuItems { get; set; }
            
            public StudentPageMasterViewModel()
            {
                MenuItems = new ObservableCollection<StudentPageMenuItem>(new[]
                {
                    //new StudentPageMenuItem { Id = 0, Title = "Главная" },
                    new StudentPageMenuItem { Id = 1, Title = "Выход", TargetType = typeof(LogoutPage), TextColor = Color.FromRgb(178, 51, 51)},
                    new StudentPageMenuItem { Id = 2, Title = "Wait", TargetType = typeof(WaitPage), TextColor = Color.MediumAquamarine},
                });
            }
            
            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}