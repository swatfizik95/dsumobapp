﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DsuMobApp.Views.Student
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StudentPageDetail : TabbedPage
    {
        public StudentPageDetail ()
        {
            InitializeComponent();
        }
    }
}