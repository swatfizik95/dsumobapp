﻿using System;
using Xamarin.Forms;

namespace DsuMobApp.Views.Student
{

    public class StudentPageMenuItem
    {
        public StudentPageMenuItem()
        {
            TargetType = typeof(StudentPageDetail);
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public Color TextColor { get; set; }
        public Type TargetType { get; set; }
    }
}