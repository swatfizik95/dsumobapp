﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using DsuMobApp.Converters;
using DsuMobApp.Model;

namespace DsuMobApp.ViewModels
{
    public class VoteViewModel : BaseViewModel
    {
        private string _text;
        private DateTime _createdAt;
        private int? _voteStatus;
        private ObservableCollection<VoteAnswerViewModel> _answers;
        private UserViewModel _createdBy;

        public string Text
        {
            get => _text;
            set
            {
                _text = value; 
                OnPropertyChanged();
                OnPropertyChanged(nameof(TextWithSpaces));
            }
        }

        public string TextWithSpaces => Converter.ToText(Text, CreatedAt);

        public DateTime CreatedAt
        {
            get => _createdAt;
            set
            {
                _createdAt = value; 
                OnPropertyChanged();
                OnPropertyChanged(nameof(CreatedAtText));
            }
        }

        public string CreatedAtText => Converter.ToText(CreatedAt);

        public int? VoteStatus
        {
            get => _voteStatus;
            set
            {
                _voteStatus = value; 
                OnPropertyChanged();
            }
        }

        public UserViewModel CreatedBy
        {
            get => _createdBy;
            set
            {
                _createdBy = value; 
                OnPropertyChanged();
            }
        }

        public ObservableCollection<VoteAnswerViewModel> Answers
        {
            get => _answers;
            set
            {
                _answers = value; 
                OnPropertyChanged();
            }
        }

        public VoteViewModel(Vote vote)
        {
            Id = vote.Id;
            VoteStatus = vote.VoteStatus;
            CreatedAt = vote.CreatedAt;
            Text = vote.Text;
            CreatedBy = new UserViewModel(vote.CreatedBy);

            Answers = new ObservableCollection<VoteAnswerViewModel>(
                vote.Answers.Select((a, index) => new VoteAnswerViewModel
                {
                    Number = index + 1,
                    Id = a.Id,
                    Text = a.Text,
                    Count = vote.UserAnswer.FirstOrDefault(
                                ua => ua.AnswerId == a.Id)?.Count?? 0,
                    IsUserAnswer = vote.Delivery.AnswerId == a.Id
                }));
        }

        public void Update(Vote vote)
        {
            Id = vote.Id;
            VoteStatus = vote.VoteStatus;
            CreatedAt = vote.CreatedAt;
            Text = vote.Text;
            CreatedBy = new UserViewModel(vote.CreatedBy);

            Answers = new ObservableCollection<VoteAnswerViewModel>(
                vote.Answers.Select((a, index) => new VoteAnswerViewModel
                {
                    Number = index + 1,
                    Id = a.Id,
                    Text = a.Text,
                    Count = vote.UserAnswer.FirstOrDefault(
                                ua => ua.AnswerId == a.Id)?.Count ?? 0,
                    IsUserAnswer = vote.Delivery.AnswerId == a.Id
                }));
        }
    }
}