﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using DsuMobApp.Annotations;
using Xamarin.Forms;

namespace DsuMobApp.ViewModels
{
    public class TestViewModel : BasePageViewModel
    {
        private string _message;

        public string Message
        {
            get => _message;
            set
            {
                _message = value; 
                OnPropertyChanged(nameof(Message));
            }
        }

        public ICommand TestCommand => new Command(() =>
        {
            Application.Current.MainPage.DisplayAlert("testVM", "testVM", "aaVM");
        });

        //public event PropertyChangedEventHandler PropertyChanged;

        //[NotifyPropertyChangedInvocator]
        //protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        //}
    }
}