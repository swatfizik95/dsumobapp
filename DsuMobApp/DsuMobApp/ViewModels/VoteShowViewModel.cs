﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using DsuMobApp.Model;
using DsuMobApp.Model.Request;
using Plugin.LocalNotification;
using Xamarin.Forms;

namespace DsuMobApp.ViewModels
{
    public class VoteShowViewModel : BasePageViewModel
    {
        private VoteViewModel _vote;

        public VoteViewModel Vote
        {
            get => _vote;
            set
            {
                _vote = value; 
                OnPropertyChanged();
            }
        }

        public ICommand SendAnswerCommand => new Command(async item =>
        {
            if (item is VoteAnswerViewModel answer)
            {
                IsBusy = true;

                var vote = (await Api.VotesAnswerAsync(Vote.Id, new VoteAnswerRequest
                {
                    Answer = new VoteAnswer
                    {
                        Id = answer.Id
                    }
                })).Vote;

                if (vote != null)
                {
                    Vote.Update(vote);
                }

                IsBusy = false;
            }
        });

        #region Constructors

        public VoteShowViewModel(Vote vote)
        {
            Vote = new VoteViewModel(vote);
        }

        public VoteShowViewModel(VoteViewModel vote)
        {
            Vote = vote;
        }

        #endregion
    }
}