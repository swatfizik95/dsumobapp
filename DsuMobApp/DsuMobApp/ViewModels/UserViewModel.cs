﻿using DsuMobApp.Model;

namespace DsuMobApp.ViewModels
{
    public class UserViewModel : BaseViewModel
    {
        private string _login;

        public string Login
        {
            get => _login;
            set
            {
                _login = value; 
                OnPropertyChanged();
            }
        }

        public UserViewModel(User user)
        {
            Id = user.Id;
            Login = user.Login;
        }
    }
}