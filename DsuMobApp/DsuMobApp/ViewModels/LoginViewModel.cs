﻿using System.Threading.Tasks;
using DsuMobApp.Model.Request;
using DsuMobApp.Model.Response;
using DsuMobApp.Services;
using Xamarin.Forms;

namespace DsuMobApp.ViewModels
{
    public class LoginViewModel : BasePageViewModel
    {
        #region Variables

        private string _login;
        private string _password;

        #endregion

        #region Constructors

        public LoginViewModel()
        {
            AuthAttemptCommand = new Command(
                async () => await AuthAttempt(),
                () => IsBusy);
        }

        #endregion

        #region Properties

        public string Login
        {
            get => _login;
            set
            {
                _login = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

        public Command AuthAttemptCommand { get; }

        #endregion

        #region Methods

        private async Task AuthAttempt()
        {
            IsBusy = true;

            var response = await Api.LoginAsync(new LoginRequest
            {
                Login = Login,
                Password = Password
            });

            if (response == null ||
                response.Message == BaseResponse.Error ||
                response.AccessToken == null)
            {
                await Application.Current.MainPage.DisplayAlert("Внимание", "Ошибка авторизации!", "Ok");
            }
            else
            {
                var token = new TokenService();
                await token.SaveTokenAsync(response.AccessToken);
                var roles = await Api.RolesAsync();
                var navigate = new NavigateService();
                navigate.ChangeMainPage(roles);
            }

            IsBusy = false;
        }

        #endregion
    }
}