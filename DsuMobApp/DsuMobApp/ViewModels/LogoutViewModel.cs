﻿using System.Threading.Tasks;
using DsuMobApp.Model.Response;
using DsuMobApp.Services;
using DsuMobApp.Views;
using Xamarin.Forms;

namespace DsuMobApp.ViewModels
{
    public class LogoutViewModel : BasePageViewModel
    {
        public Command LogoutCommand { get; }

        public LogoutViewModel()
        {
            LogoutCommand = new Command(async () => await LogoutAsync());
            StartLogout();
        }

        #region Methods

        private async Task LogoutAsync()
        {
            IsBusy = true;

            var response = await Api.LogoutAsync();

            if (response != null &&
                response.Message == BaseResponse.Ok)
            {
                var token = new TokenService();
                token.Revoke();
                Application.Current.MainPage = new LoginPage();
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Ошибка", "Не удалось выйти", "Ok");
            }

            IsBusy = false;
        }

        private async void StartLogout()
        {
            IsBusy = true;

            await Task.Delay(2000);

            var api = new ApiService();
            var response = await api.LogoutAsync();

            if (response != null &&
                response.Message == BaseResponse.Ok)
            {
                var token = new TokenService();
                token.Revoke();
                Application.Current.MainPage = new LoginPage();
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Ошибка", "Не удалось выйти", "Ok");
            }

            IsBusy = false;
        }

        #endregion
    }
}
