﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using DsuMobApp.Annotations;
using DsuMobApp.Services;

namespace DsuMobApp.ViewModels
{
    public abstract class BasePageViewModel : INotifyPropertyChanged
    {
        internal ApiService Api = new ApiService();

        private bool _isBusy;

        #region Properties

        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                _isBusy = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsEnabled));
            }
        }

        public bool IsEnabled => !IsBusy;

        #endregion

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
