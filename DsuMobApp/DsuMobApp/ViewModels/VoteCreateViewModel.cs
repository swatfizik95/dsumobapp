﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DsuMobApp.Model;
using DsuMobApp.Model.Request;
using DsuMobApp.Model.Response;
using DsuMobApp.Services;
using Xamarin.Forms;

namespace DsuMobApp.ViewModels
{
    public class VoteCreateViewModel : BasePageViewModel
    {
        private const int ROW_HEIGHT = 50;

        public int RowHeight => ROW_HEIGHT;

        public int RolesHeight => RolesToChoose.Count * ROW_HEIGHT;

        public int GroupsHeight => GroupsToChoose.Count * ROW_HEIGHT;
        //GroupsToChoose.Count * ROW_HEIGHT > ROW_HEIGHT_MAX ? ROW_HEIGHT_MAX : GroupsToChoose.Count * ROW_HEIGHT;

        public int CoursesHeight => CoursesToChoose.Count * ROW_HEIGHT;
        //CoursesToChoose.Count * ROW_HEIGHT > ROW_HEIGHT_MAX ? ROW_HEIGHT_MAX : CoursesToChoose.Count * ROW_HEIGHT;

        public int AnswersHeight => Answers.Count * ROW_HEIGHT;

        #region Variables

        private string _text;
        private List<Role> _rolesInitial;
        private ObservableCollection<Role> _rolesToChoose = new ObservableCollection<Role>();
        private List<Role> _roles = new List<Role>();
        private List<Group> _groupsInitial;
        private ObservableCollection<Group> _groupsToChoose = new ObservableCollection<Group>();
        private List<Group> _groups = new List<Group>();
        private List<Course> _coursesInitial;
        private ObservableCollection<Course> _coursesToChoose = new ObservableCollection<Course>();
        private List<Course> _courses = new List<Course>();
        private ObservableCollection<VoteAnswer> _answers;
        private string _answerText;

        #endregion

        #region Properties

        public string Text
        {
            get => _text;
            set
            {
                _text = value;
                OnPropertyChanged();
            }
        }

        #region Roles

        public bool IsRolesFilled => _roles.Count > 0;

        public string RolesClearImageSource => _roles.Count > 0
            ? ResourceService.DELETE_IMG
            : ResourceService.DELETE_DISABLED_IMG;

        public string RolesNames => _roles.Count > 0
            ? string.Join(", ", _roles.Select(g => g.Description))
            : "Роли не выбраны";

        public ObservableCollection<Role> RolesToChoose
        {
            get => _rolesToChoose;
            set
            {
                _rolesToChoose = value;
                OnPropertyChanged();
                RolesChanged();
            }
        }

        public Command RolesAddCommand { get; }
        public Command RolesClearCommand { get; }

        #endregion

        #region Groups

        public bool IsGroupsFilled => _groups.Count > 0;

        public string GroupsClearImageSource => _groups.Count > 0
            ? ResourceService.DELETE_IMG
            : ResourceService.DELETE_DISABLED_IMG;

        public string GroupsNames => _groups.Count > 0
            ? string.Join(", ", _groups.Select(g => g.Name))
            : "Группы не выбраны";

        public ObservableCollection<Group> GroupsToChoose
        {
            get => _groupsToChoose;
            set
            {
                _groupsToChoose = value;
                OnPropertyChanged();
                GroupsChanged();
            }
        }

        public Command GroupsAddCommand { get; }
        public Command GroupsClearCommand { get; }

        #endregion

        #region Courses

        public bool IsCoursesFilled => _courses.Count > 0;

        public string CoursesClearImageSource => _courses.Count > 0
            ? ResourceService.DELETE_IMG
            : ResourceService.DELETE_DISABLED_IMG;

        public string CoursesNumbers => _courses.Count > 0
            ? string.Join(", ", _courses.Select(g => g.Number))
            : "Курсы не выбраны";

        public ObservableCollection<Course> CoursesToChoose
        {
            get => _coursesToChoose;
            set
            {
                _coursesToChoose = value;
                OnPropertyChanged();
                CourseChanged();
            }
        }

        public Command CoursesAddCommand { get; }
        public Command CoursesClearCommand { get; }

        #endregion

        #region Answers

        public ObservableCollection<VoteAnswer> Answers
        {
            get => _answers;
            set
            {
                _answers = value; 
                OnPropertyChanged();
                OnPropertyChanged(nameof(AnswersHeight));
            }
        }

        public string AnswerText
        {
            get => _answerText;
            set
            {
                _answerText = value; 
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsAnswerCorrect));
                OnPropertyChanged(nameof(AnswerAddImageSource));
            }
        }

        public bool IsAnswerCorrect => !string.IsNullOrWhiteSpace(AnswerText);

        public string AnswerAddImageSource => IsAnswerCorrect
            ? ResourceService.PLUS_IMG
            : ResourceService.PLUS_DISABLED_IMG;

        public Command AnswerAddCommand { get; }
        public Command AnswerRemoveCommand { get; }

        #endregion

        public Command SendCommand { get; }

        #endregion

        #region Constructor

        public VoteCreateViewModel()
        {
            Answers = new ObservableCollection<VoteAnswer>();
            AnswerAddCommand = new Command(AnswersAdd);
            AnswerRemoveCommand = new Command(AnswersRemove);


            RolesAddCommand = new Command(RolesAdd);
            RolesClearCommand = new Command(RolesClear);

            GroupsAddCommand = new Command(GroupsAdd);
            GroupsClearCommand = new Command(GroupsClear);

            CoursesAddCommand = new Command(CoursesAdd);
            CoursesClearCommand = new Command(CoursesClear);

            SendCommand = new Command(async () => await Send());

            StartLoadRoles();
            StartLoadGroups();
            StartLoadCourses();
        }

        #endregion

        #region Methods

        #region Roles

        private void RolesAdd(object item)
        {
            if (item is Role role)
            {
                if (!_roles.Contains(role))
                {
                    _roles.Add(role);
                    RolesToChoose.Remove(role);
                    RolesChanged();
                }
            }
        }

        private void RolesClear()
        {
            _roles.Clear();
            RolesToChoose = new ObservableCollection<Role>(_rolesInitial);
        }

        private async void StartLoadRoles()
        {
            _rolesInitial = new List<Role>((await Api.RolesStudentsAsync()).Roles);
            RolesToChoose = new ObservableCollection<Role>(_rolesInitial);
        }

        #endregion

        #region Groups

        private void GroupsAdd(object item)
        {
            if (item is Group group)
            {
                if (!_groups.Contains(group))
                {
                    _groups.Add(group);
                    GroupsToChoose.Remove(group);
                    GroupsChanged();
                }
            }
        }

        private void GroupsClear()
        {
            _groups.Clear();
            GroupsToChoose = new ObservableCollection<Group>(_groupsInitial);
        }

        private async void StartLoadGroups()
        {
            _groupsInitial = new List<Group>((await Api.GroupsAsync()).Groups);
            GroupsToChoose = new ObservableCollection<Group>(_groupsInitial);
        }

        #endregion

        #region Courses

        private void CoursesAdd(object item)
        {
            if (item is Course course)
            {
                if (!_courses.Contains(course))
                {
                    _courses.Add(course);
                    CoursesToChoose.Remove(course);
                    CourseChanged();
                }
            }
        }

        private void CoursesClear()
        {
            _courses.Clear();
            CoursesToChoose = new ObservableCollection<Course>(_coursesInitial);
        }

        private async void StartLoadCourses()
        {
            _coursesInitial = new List<Course>((await Api.CoursesAsync()).Courses);
            CoursesToChoose = new ObservableCollection<Course>(_coursesInitial);
        }

        #endregion

        #region Answers

        private void AnswersAdd()
        {
            Answers.Add(new VoteAnswer
            {
                Id = Answers.Count + 1,
                Text = AnswerText.Trim()
            });

            AnswersChanged();
            
            AnswerText = string.Empty;
        }

        private void AnswersRemove(object item)
        {
            if (item is VoteAnswer answer)
            {
                Answers = new ObservableCollection<VoteAnswer>(Answers
                    .Where(a => a.Id != answer.Id)
                    .Select((a, index) => new VoteAnswer
                    {
                        Id = index + 1,
                        Text = a.Text
                    }));
            }
        }

        private void AnswersClear()
        {
            Answers.Clear();
            AnswersChanged();
        }

        #endregion

        private async Task Send()
        {
            IsBusy = true;

            string title = "Внимание";
            string displayMessage = null;

            if (string.IsNullOrWhiteSpace(_text))
            {
                displayMessage = "Введите сообщение!";
            }
            else if (_groups.Count < 1)
            {
                displayMessage = "Выберите группу!";
            }
            else if (_courses.Count < 1)
            {
                displayMessage = "Выберите курсы!";
            }
            else if (_rolesToChoose.Count < 1)
            {
                displayMessage = "Выберите роли!";
            }
            else if (_answers.Count < 2)
            {
                displayMessage = "Количество вариантов ответа меньше 2х!";
            }
            else
            {
                int[] groups = _groups.Select(g => g.Id).ToArray();
                int[] courses = _courses.Select(c => c.Id).ToArray();
                int[] roles = _roles.Select(r => r.Id).ToArray();
                VoteAnswer[] answers = _answers.Select(a => new VoteAnswer {Text = a.Text}).ToArray();

                var response = await Api.CreateVoteAsync(new VoteRequest
                {
                    Text = _text,
                    Groups = groups,
                    Courses = courses,
                    Roles = roles,
                    Answers = answers
                });

                if (response.Message == BaseResponse.Ok)
                {
                    title = "Успешно";
                    displayMessage = "Голосование успешно разослано!";
                    GroupsClear();
                    CoursesClear();
                    RolesClear();
                    AnswersClear();
                    Text = string.Empty;
                }
                else
                {
                    title = "Ошибка";
                    displayMessage = "Голосование не удалось разослать!\r\n" +
                                     "Пожалуйста, повторите попытку позже";
                }
            }

            if (!string.IsNullOrWhiteSpace(displayMessage))
            {
                await Application.Current.MainPage.DisplayAlert(title, displayMessage, "Ok");
            }

            IsBusy = false;
        }

        private void RolesChanged()
        {
            OnPropertyChanged(nameof(IsRolesFilled));
            OnPropertyChanged(nameof(RolesClearImageSource));
            OnPropertyChanged(nameof(RolesNames));
            OnPropertyChanged(nameof(RolesHeight));
        }

        private void GroupsChanged()
        {
            OnPropertyChanged(nameof(IsGroupsFilled));
            OnPropertyChanged(nameof(GroupsClearImageSource));
            OnPropertyChanged(nameof(GroupsNames));
            OnPropertyChanged(nameof(GroupsHeight));
        }

        private void CourseChanged()
        {
            OnPropertyChanged(nameof(IsCoursesFilled));
            OnPropertyChanged(nameof(CoursesClearImageSource));
            OnPropertyChanged(nameof(CoursesNumbers));
            OnPropertyChanged(nameof(CoursesHeight));
        }

        private void AnswersChanged()
        {
            OnPropertyChanged(nameof(AnswersHeight));
        }

        #endregion
    }
}