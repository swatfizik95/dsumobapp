﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DsuMobApp.Model;
using DsuMobApp.Services;
using DsuMobApp.Views;
using Xamarin.Forms;

namespace DsuMobApp.ViewModels
{
    public class NotificationsViewModel : BasePageViewModel, IDisposable
    {
        private INavigation _navigation;

        private const int ROW_HEIGHT = 80;
        public int RowHeight => ROW_HEIGHT;

        #region Variables

        private ObservableCollection<Notification> _notifications = 
            new ObservableCollection<Notification>();

        #endregion

        #region Properties

        public ObservableCollection<Notification> Notifications
        {
            get => _notifications;
            set
            {
                _notifications = value;
                OnPropertyChanged();
            }
        }

        public Command NotificationShowCommand => new Command(async item =>
        {
            IsBusy = true;
            var notification = item as Notification;
            await _navigation.PushAsync(new NotificationPage(notification));
            IsBusy = false;
        });

        #endregion

        #region Constructors

        public NotificationsViewModel(INavigation navigation)
        {
            _navigation = navigation;
            _cancellationTokenSource = new CancellationTokenSource();

            StartLoadNotifications();
        }

        #endregion

        #region Methods

        private async void StartLoadNotifications()
        {
            Notifications = new ObservableCollection<Notification>((await Api.NotificationsReadedAsync()).Notifications);
            
            StartLoadUnreadedNotifications(_cancellationTokenSource.Token);
        }

        private readonly CancellationTokenSource _cancellationTokenSource;

        private void StartLoadUnreadedNotifications(CancellationToken cancellationToken)
        {
            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    await Task.Delay(1000, cancellationToken);

                    var notifications = (await Api.NotificationsUnreadedAsync()).Notifications;
                    if (notifications.Any())
                    {
                        foreach (var notification in notifications)
                        {
                            Notifications.Insert(0, notification);
                        }
                    }

                    if (_cancellationTokenSource.IsCancellationRequested)
                    {
                        return;
                    }
                }
            }, cancellationToken);
        }

        public void Dispose()
        {
            _cancellationTokenSource.Cancel();
        }

        #endregion
    }
}