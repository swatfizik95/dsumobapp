﻿using DsuMobApp.Model;

namespace DsuMobApp.ViewModels
{
    public class NotificationViewModel : BasePageViewModel
    {
        private Notification _notification;

        public Notification Notification
        {
            get => _notification;
            set
            {
                _notification = value; 
                OnPropertyChanged();
            }
        }

        public NotificationViewModel(Notification notification)
        {
            Notification = notification;
        }
    }
}