﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DsuMobApp.Views;
using Xamarin.Forms;

namespace DsuMobApp.ViewModels
{
    public class VoteListViewModel : BasePageViewModel
    {
        private INavigation _navigation;

        private ObservableCollection<VoteViewModel> _votes;

        public ObservableCollection<VoteViewModel> Votes
        {
            get => _votes;
            set
            {
                _votes = value; 
                OnPropertyChanged();
            }
        }

        public Command VoteShowCommand => new Command(async item =>
        {
            IsBusy = true;
            var vote = item as VoteViewModel;
            await _navigation.PushAsync(new VoteShowPage(vote));
            IsBusy = false;
        });

        public VoteListViewModel(INavigation navigation)
        {
            _navigation = navigation;
            _ctsUnreaded = new CancellationTokenSource();
            _ctsUpdated = new CancellationTokenSource();

            StartLoadVotes();
        }

        #region Methods

        private readonly CancellationTokenSource _ctsUnreaded;
        private readonly CancellationTokenSource _ctsUpdated;
        private async void StartLoadVotes()
        {
            await StartLoadReadVotes();
            StartLoadUnreadVotes(_ctsUnreaded.Token);
            StartLoadUpdatedVotes(_ctsUpdated.Token);
        }

        private async Task StartLoadReadVotes()
        {
            var votes = (await Api.VotesReadAsync()).Votes;
            Votes = votes.Any()
                ? new ObservableCollection<VoteViewModel>(votes.Select(v => new VoteViewModel(v)))
                : new ObservableCollection<VoteViewModel>();
        }

        private void StartLoadUnreadVotes(CancellationToken token)
        {
            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    await Task.Delay(1000, token);

                    var votes = (await Api.VotesUnreadAsync(token)).Votes;
                    if (votes.Any())
                    {
                        foreach (var vote in votes)
                        {
                            Votes.Insert(0, new VoteViewModel(vote));
                        }
                    }

                    if (token.IsCancellationRequested)
                    {
                        return;
                    }
                }
            }, token);
        }

        private void StartLoadUpdatedVotes(CancellationToken token)
        {
            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    await Task.Delay(1000, token);

                    var votes = (await Api.VotesUpdatedAsync(token)).Votes;
                    if (votes.Any())
                    {
                        foreach (var vote in votes)
                        {
                            Votes.FirstOrDefault(v => v.Id == vote.Id)?.Update(vote);
                        }
                    }

                    if (token.IsCancellationRequested)
                    {
                        return;
                    }
                }
            }, token);
        }

        #endregion
    }
}