﻿namespace DsuMobApp.ViewModels
{
    public class VoteAnswerViewModel : BaseViewModel
    {
        private int _number;
        private int _count;
        private string _text;
        private bool _isUserAnswer;

        public int Number
        {
            get => _number;
            set
            {
                _number = value; 
                OnPropertyChanged();
            }
        }

        public int Count
        {
            get => _count;
            set
            {
                _count = value; 
                OnPropertyChanged();
            }
        }

        public string Text
        {
            get => _text;
            set
            {
                _text = value; 
                OnPropertyChanged();
            }
        }

        public bool IsUserAnswer
        {
            get => _isUserAnswer;
            set
            {
                _isUserAnswer = value; 
                OnPropertyChanged();
            }
        }
    }
}