﻿using System.Windows.Input;
using Xamarin.Forms;

namespace DsuMobApp.Elements
{
    public class MyListView : ListView
    {
        public static BindableProperty ClickCommandProperty = BindableProperty.Create(
            nameof(ClickCommand),
            typeof(ICommand),
            typeof(MyListView));

        public ICommand ClickCommand
        {
            get => (ICommand) GetValue(ClickCommandProperty);
            set => SetValue(ClickCommandProperty, value);
        }

        public MyListView()
        {
            ItemTapped += OnItemTapped;
        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item != null)
            {
                ClickCommand?.Execute(e.Item);
                SelectedItem = null;
            }
        }
    }
}
