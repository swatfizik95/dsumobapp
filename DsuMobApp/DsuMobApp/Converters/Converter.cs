﻿using System;

namespace DsuMobApp.Converters
{
    public class Converter
    {
        public static string ToText(DateTime dateTime)
        {
            if (dateTime.Date == DateTime.Today)
            {
                return $"{dateTime:HH:mm}";
            }

            if ((DateTime.Today - dateTime.Date).Days == 1)
            {
                return "Вчера";
            }

            return $"{dateTime:MM.dd.yy}";
        }

        public static string ToText(string text, DateTime dateTime)
        {
            return $"{text}{new string(' ', ToText(dateTime).Length * 2)}";
        }
    }
}