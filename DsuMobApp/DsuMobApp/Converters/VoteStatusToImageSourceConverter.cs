﻿using System;
using System.Globalization;
using DsuMobApp.Model;
using DsuMobApp.Services;
using Xamarin.Forms;

namespace DsuMobApp.Converters
{
    public class VoteStatusToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((int) value)
            {
                case Status.VOTE_FINISHED:
                    return ResourceService.CHECKED_IMG;
                case Status.VOTE_STARTED:
                    return ResourceService.SYNC_IMG;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((string) value)
            {
                case ResourceService.CHECKED_IMG:
                    return Status.VOTE_FINISHED;
                case ResourceService.SYNC_IMG:
                    return Status.VOTE_STARTED;
            }

            return -1;
        }
    }
}
