﻿using System;
using System.Collections;
using System.Globalization;
using Xamarin.Forms;

namespace DsuMobApp.Converters
{
    public class CollectionCountToHeightConverter : IValueConverter
    {
        private const int ROW_HEIGHT = 40;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is IList list)
            {
                if (parameter is int rowHeight)
                {
                    return list.Count * rowHeight;
                }

                return list.Count * ROW_HEIGHT;
            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}