﻿using System;
using System.Globalization;
using DsuMobApp.Services;
using Xamarin.Forms;

namespace DsuMobApp.Converters
{
    public class IsUserAnswerToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool isUserAnswer && isUserAnswer)
            {
                return ResourceService.CHECKED_IMG;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}