﻿using System;
using System.Threading.Tasks;
using DsuMobApp.Services;
using DsuMobApp.Views;
using DsuMobApp.Views.Admin;
using DsuMobApp.Views.Student;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace DsuMobApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new WaitPage();
            //MainPage = new TestPage();
        }

        protected override async void OnStart()
        {
            var tokenService = new TokenService();
            if (await tokenService.LoadTokenAsync() != null)
            {
                //MainPage = new VoteCreatePage();

                var api = new ApiService();
                var roles = await api.RolesAsync();
                var navigate = new NavigateService();
                navigate.ChangeMainPage(roles);
            }
            else
            {
                MainPage = new LoginPage();
            }
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
