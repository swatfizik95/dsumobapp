﻿namespace DsuMobApp.Services
{
    public class ResourceService
    {
        //public const string CANCEL_IMG = "cancel.png";
        //public const string CANCEL_DISABLED_IMG = "cancel_disabled.png";
        //public const string ADD_IMG = "add.png";
        //public const string ADD_DISABLED_IMG = "add_disabled.png";
        public const string PLUS_IMG = "plus.png";
        public const string PLUS_DISABLED_IMG = "plus_disabled.png";
        public const string DELETE_IMG = "delete.png";
        public const string DELETE_DISABLED_IMG = "delete_disabled.png";
        public const string CHECKED_IMG = "checked.png";
        public const string SYNC_IMG = "sync.png";
    }
}