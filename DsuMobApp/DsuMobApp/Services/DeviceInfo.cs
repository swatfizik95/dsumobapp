﻿using Xamarin.Forms;

namespace DsuMobApp.Services
{
    public class DeviceInfo : IDeviceInfo
    {
        readonly IDeviceInfo _deviceInfo = DependencyService.Get<IDeviceInfo>();

        public string GetInfo()
        {
            return _deviceInfo.GetInfo();
        }
    }
}