﻿namespace DsuMobApp.Services
{
    public interface IDeviceInfo
    {
        string GetInfo();
    }
}