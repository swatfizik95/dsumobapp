﻿using System;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace DsuMobApp.Services
{
    public class TokenService
    {
        private const string TOKEN_STORAGE = "oauth_token";
        private static string _token = null;

        public static string Token => _token;

        #region Methods

        public async Task SaveTokenAsync(string token)
        {
            try
            {
                _token = token;
                await SecureStorage.SetAsync(TOKEN_STORAGE, _token);
            }
            catch (Exception)
            {
                // Possible that device doesn't support secure storage on device.
            }
        }

        public async Task<string> LoadTokenAsync()
        {
            try
            {
                _token = await SecureStorage.GetAsync(TOKEN_STORAGE);
            }
            catch (Exception)
            {
                _token = null;
                // Possible that device doesn't support secure storage on device.
            }

            return _token;
        }

        public bool Revoke()
        {
            try
            {
                _token = null;
                return SecureStorage.Remove(TOKEN_STORAGE);
            }
            catch (Exception)
            {
                return false;
                // Possible that device doesn't support secure storage on device.
            }
        }

        #endregion
    }
}
