﻿using System;
using System.Linq;
using DsuMobApp.Model;
using DsuMobApp.Views;
using DsuMobApp.Views.Admin;
using DsuMobApp.Views.Student;
using Xamarin.Forms;

namespace DsuMobApp.Services
{
    public class NavigateService
    {
        public void ChangeMainPage(string[] roles)
        {
            if (roles == null || roles.Length < 1)
            {
                Application.Current.MainPage = new LoginPage();
            }
            else if (roles.Any(adminRoles => new[] { Role.ADMIN }.Contains(adminRoles)))
            {
                Application.Current.MainPage = new AdminPage();
            }
            else if (roles.Contains(Role.STUDENT))
            {
                Application.Current.MainPage = new StudentPage();
            }

            //throw new Exception("Не найден page для этих ролей");
        }
    }
}
