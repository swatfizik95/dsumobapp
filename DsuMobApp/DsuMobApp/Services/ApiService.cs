﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DsuMobApp.Model;
using DsuMobApp.Model.Request;
using DsuMobApp.Model.Response;
using DsuMobApp.Serialization;
using DsuMobApp.ViewModels;
using Newtonsoft.Json;
using RestSharp;

namespace DsuMobApp.Services
{
    public class ApiService
    {
        //private const string URL = "http://dsu";
        private const string URL = "http://46.101.223.127";
        private const string API = "api";
        

        private static readonly RestClient Client = new RestClient(URL);

        #region Methods

        public async Task<LoginResponse> LoginAsync(LoginRequest model)
        {
            return await ExecuteAsync<LoginResponse>("login", false, Method.POST, model);
        }

        public async Task<LogoutResponse> LogoutAsync()
        {
            return await ExecuteAsync<LogoutResponse>("logout");
        }

        public async Task<GroupsResponse> GroupsAsync()
        {
            return await ExecuteAsync<GroupsResponse>("groups");
        }

        public async Task<CoursesResponse> CoursesAsync()
        {
            return await ExecuteAsync<CoursesResponse>("courses");
        }

        #region Notification

        public async Task<BaseResponse> CreateNotificationAsync(NotificationRequest model)
        {
            return await ExecuteAsync<BaseResponse>("notifications/create", true, Method.POST, model);
        }

        public async Task<NotificationsResponse> NotificationsReadedAsync()
        {
            return await ExecuteAsync<NotificationsResponse>("notifications/readed");
        }

        public async Task<NotificationsResponse> NotificationsUnreadedAsync()
        {
            return await ExecuteAsync<NotificationsResponse>("notifications/unreaded");
        }

        #endregion

        #region Votes

        public async Task<BaseResponse> CreateVoteAsync(VoteRequest model)
        {
            return await ExecuteAsync<BaseResponse>("votes/create", true, Method.POST, model);
        }

        public async Task<VotesResponse> VotesReadAsync()
        {
            return await ExecuteAsync<VotesResponse>("votes/readed");
        }

        public async Task<VotesResponse> VotesUnreadAsync(CancellationToken token)
        {
            return await ExecuteAsync<VotesResponse>("votes/unreaded", token);
        }

        public async Task<VotesResponse> VotesUpdatedAsync(CancellationToken token)
        {
            return await ExecuteAsync<VotesResponse>("votes/updated", token);
        }

        public async Task<VoteResponse> VotesAnswerAsync(int id, VoteAnswerRequest model)
        {
            return await ExecuteAsync<VoteResponse>($"votes/{id}/answer", true, Method.POST, model);
        }

        #endregion

        #region Additional Methods

        public async Task<string[]> RolesAsync()
        {
            return await ExecuteAsync<string[]>("roles");
        }

        public async Task<RoleResponse> RolesStudentsAsync()
        {
            return await ExecuteAsync<RoleResponse>("roles/students");
        }

        public async Task<User> UserAsync()
        {
            return await ExecuteAsync<User>("user");
        }

        #endregion

        #region Method Helpers

        private async Task<T> ExecuteAsync<T>(string url, bool withAccessToken = true, Method method = Method.GET, object requestJsonBody = null)
        {
            return await ExecuteAsync<T>(url, CancellationToken.None, withAccessToken, method, requestJsonBody);
        }

        private async Task<T> ExecuteAsync<T>(string url, CancellationToken token, bool withToken = true, Method method = Method.GET, object requestJsonBody = null)
        {
            var request = CreateRequest($"{API}/{url}", method);

            if (withToken)
            {
                request.AddHeader("Authorization", $"Bearer {TokenService.Token}");
            }

            if (requestJsonBody != null)
            {
                request.AddJsonBody(requestJsonBody);
            }

            var response = await Client.ExecuteTaskAsync(request, token);

            return response.IsSuccessful ? JsonConvert.DeserializeObject<T>(response.Content) : default(T);
        }

        private RestRequest CreateRequest(string resource, Method method = Method.GET)
        {
            return new RestRequest(resource, method)
            {
                RequestFormat = DataFormat.Json,
                JsonSerializer = NewtonsoftJsonSerializer.LowerCase
            };
        }

        #endregion

        #endregion
    }
}
