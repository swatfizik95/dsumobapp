﻿using System;
using Newtonsoft.Json;

namespace DsuMobApp.Model
{
    public class Vote : BaseModel
    {
        public string Text { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty(PropertyName = "vote_status_id")]
        public int VoteStatus { get; set; }

        [JsonProperty(PropertyName = "created_by")]
        public User CreatedBy { get; set; }

        public VoteDelivery Delivery { get; set; }

        public VoteAnswer Answer { get; set; }
        public VoteAnswer[] Answers { get; set; }
        [JsonProperty(PropertyName = "user_answers")]
        public VoteUserAnswer[] UserAnswer { get; set; }
    }
}