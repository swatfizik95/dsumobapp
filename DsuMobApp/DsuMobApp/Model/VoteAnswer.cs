﻿namespace DsuMobApp.Model
{
    public class VoteAnswer : BaseModel
    {
        public string Text { get; set; }
    }
}