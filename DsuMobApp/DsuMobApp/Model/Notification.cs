﻿using System;
using DsuMobApp.Serialization;
using Newtonsoft.Json;

namespace DsuMobApp.Model
{
    public class Notification : BaseModel
    {
        public string Message { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty(PropertyName = "created_by")]
        public User CreatedBy { get; set; }

        public NotificationDelivery Delivery { get; set; }

        [JsonIgnore]
        public string CreatedAtText {
            get
            {
                if (CreatedAt.Date == DateTime.Today)
                {
                    return $"{CreatedAt:HH:mm}";
                }

                if ((DateTime.Today - CreatedAt.Date).Days == 1)
                {
                    return "Вчера";
                }

                return $"{CreatedAt:MM.dd.yy}";
            }}

        [JsonIgnore]
        public string MessageWithSpaces => $"{Message} {new string(' ', CreatedAtText.Length * 2)}";
    }
}