﻿using Newtonsoft.Json;

namespace DsuMobApp.Model.Response
{
    public class BaseResponse
    {
        public const string Ok = "ok";
        public const string Error = "error";

        public string Message { get; set; }
        [JsonProperty(PropertyName = "inner_message")]
        public string InnerMessage { get; set; }
    }
}
