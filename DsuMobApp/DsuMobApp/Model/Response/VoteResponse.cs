﻿namespace DsuMobApp.Model.Response
{
    public class VoteResponse : BaseResponse
    {
        public Vote Vote { get; set; }
    }
}