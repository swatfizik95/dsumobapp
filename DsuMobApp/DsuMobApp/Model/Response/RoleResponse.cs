﻿namespace DsuMobApp.Model.Response
{
    public class RoleResponse : BaseResponse
    {
        public Role[] Roles { get; set; }
    }
}