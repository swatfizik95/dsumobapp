﻿using System.Collections.Generic;

namespace DsuMobApp.Model.Response
{
    public class CoursesResponse : BaseResponse
    {
        public Course[] Courses { get; set; }
    }
}