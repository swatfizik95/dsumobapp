﻿namespace DsuMobApp.Model.Response
{
    public class NotificationsResponse
    {
        public Notification[] Notifications { get; set; }
    }
}