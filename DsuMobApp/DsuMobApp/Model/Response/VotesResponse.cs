﻿namespace DsuMobApp.Model.Response
{
    public class VotesResponse : BaseResponse
    {
        public Vote[] Votes { get; set; }
    }
}