﻿using System.Collections.Generic;

namespace DsuMobApp.Model.Response
{
    public class GroupsResponse : BaseResponse
    {
        public Group[] Groups { get; set; }
    }
}