﻿namespace DsuMobApp.Model.Response
{
    public class LoginResponse : BaseResponse
    {
        public string AccessToken { get; set; }
    }
}
