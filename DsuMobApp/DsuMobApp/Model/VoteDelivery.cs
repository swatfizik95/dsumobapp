﻿using Newtonsoft.Json;

namespace DsuMobApp.Model
{
    public class VoteDelivery
    {
        [JsonProperty(PropertyName = "answer_id")]
        public int? AnswerId { get; set; }
    }
}