﻿using Newtonsoft.Json;

namespace DsuMobApp.Model
{
    public class VoteUserAnswer
    {
        [JsonProperty(PropertyName = "answer_id")]
        public int AnswerId { get; set; }
        public int Count { get; set; }
    }
}