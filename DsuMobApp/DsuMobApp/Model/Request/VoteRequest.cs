﻿namespace DsuMobApp.Model.Request
{
    public class VoteRequest
    {
        public string Text { get; set; }
        public int[] Groups { get; set; }
        public int[] Courses { get; set; }
        public int[] Roles { get; set; }
        public VoteAnswer[] Answers { get; set; }
    }
}