﻿namespace DsuMobApp.Model.Request
{
    public class VoteAnswerRequest
    {
        public VoteAnswer Answer { get; set; }
    }
}