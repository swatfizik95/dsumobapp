﻿namespace DsuMobApp.Model.Request
{
    public class NotificationRequest
    {
        public string Message { get; set; }
        public int[] Groups { get; set; }
        public int[] Courses { get; set; }
        public int[] Roles { get; set; }
    }
}