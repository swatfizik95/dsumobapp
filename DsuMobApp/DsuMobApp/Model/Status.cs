﻿namespace DsuMobApp.Model
{
    public class Status
    {
        public const int VOTE_FINISHED = 25;
        public const int VOTE_STARTED = 26;
    }
}