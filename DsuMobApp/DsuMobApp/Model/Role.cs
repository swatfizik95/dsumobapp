﻿namespace DsuMobApp.Model
{
    public class Role : BaseModel
    {
        public const string ADMIN = "admin";
        public const string STUDENT = "student";

        public string Name { get; set; }
        public string Description { get; set; }
    }
}
