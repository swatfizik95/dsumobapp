﻿using Newtonsoft.Json;

namespace DsuMobApp.Model
{
    public class NotificationDelivery
    {
        public int Id { get; set; }
        [JsonProperty(PropertyName = "delivery_status_id")]
        public int DeliveryStatusId { get; set; }
    }
}