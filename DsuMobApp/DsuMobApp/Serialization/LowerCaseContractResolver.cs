﻿using Newtonsoft.Json.Serialization;

namespace DsuMobApp.Serialization
{
    public class LowerCaseContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName.ToLower();
        }
    }
}