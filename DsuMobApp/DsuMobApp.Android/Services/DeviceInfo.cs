﻿using Android.OS;
using DsuMobApp.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(DsuMobApp.Droid.Services.DeviceInfo))]
namespace DsuMobApp.Droid.Services
{
    public class DeviceInfo : IDeviceInfo
    {
        public string GetInfo()
        {
            return $"Android {Build.VERSION.Release}";
        }
    }
}