﻿using DsuMobApp.Services;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(DsuMobApp.iOS.Services.DeviceInfo))]
namespace DsuMobApp.iOS.Services
{
    public class DeviceInfo : IDeviceInfo
    {
        public string GetInfo()
        {
            UIDevice device = new UIDevice();
            return $"iOS {device.SystemName} {device.SystemVersion}";
        }
    }
}